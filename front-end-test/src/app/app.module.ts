import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { IndexRoleComponent } from './roles/components/index/index.component';
import { CreateRoleComponent } from './roles/components/create/create.component';
import { EditRoleComponent } from './roles/components/edit/edit.component';
import { appRoutes } from './app.routing';
import { RoleService } from './service';
@NgModule({
  declarations: [
    AppComponent,
    IndexRoleComponent,
    CreateRoleComponent,
    EditRoleComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes), HttpClientModule, ReactiveFormsModule
  ],
  providers: [RoleService],
  bootstrap: [AppComponent]
})
export class AppModule { }