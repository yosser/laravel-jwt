import { Injectable } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import {Role} from './role';

@Injectable()
export class RoleService {
  rolesList : Role[];
  result: any;
  constructor(private http: HttpClient) {}

  addRole(name, display_name) {
    const uri = 'http://127.0.0.1:8000/Roles/Create';
    const obj = {
      name: name,
      display_name: display_name
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res =>
          console.log('Done'));
  }

  getRoles() {
    const uri = 'http://localhost:8000/Roles/All-Roles';
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  editRole(id) {
    const uri = 'http://localhost:8000/Roles/GetRoleById/' + id;
    return this
            .http
            .get(uri)
            .map(res => {
              return res;
            });
  }

  updateRole(name, display_name, id) {
    const uri = 'http://localhost:8000/Roles/Edit/' + id;

    const obj = {
      name: name,
      display_name: display_name
    };
    this
      .http
      .post(uri, obj)
      .subscribe(res => console.log('Done'));
  }

  deleteRole(id) {
    const uri = 'http://localhost:8000/Roles/Delete/' + id;

        return this
            .http
            .post(uri,id)
            .map(res => {
              return res;
            });
  }
}
