import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoleService } from './../../../service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditRoleComponent implements OnInit {

  role: any;
  angForm: FormGroup;
  title = 'Edit Role';
  constructor(private route: ActivatedRoute, private router: Router, private service: RoleService, private fb: FormBuilder) {
    this.createForm();
   }

  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      display_name: ['', Validators.required ]
   });
  }

  updateRole(name, display_name) {
    this.route.params.subscribe(params => {
    this.service.updateRole(name, display_name, params['id']);
    this.router.navigate(['index']);
  });
}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.role = this.service.editRole(params['id']).subscribe(res => {
        this.role = res;
      });
    });
  }
}
