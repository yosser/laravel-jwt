import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RoleService } from './../../../service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateRoleComponent implements OnInit {
  title = 'Add Role';
  angForm: FormGroup;
  constructor(private Roleservice: RoleService, private fb: FormBuilder, private router: Router) {
    this.createForm();
   }
  createForm() {
    this.angForm = this.fb.group({
      name: ['', Validators.required ],
      display_name: ['', Validators.required ]
   });
  }
  addRole(name, display_name) {
      this.Roleservice.addRole(name, display_name);
      this.router.navigate(['index']);
  }
  ngOnInit() {
  }
}
