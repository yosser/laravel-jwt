import { RoleService } from './../../../service';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Role } from '../../../role';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
  
})
export class IndexRoleComponent implements OnInit {

  roles: any;

  constructor(private http: HttpClient, private service: RoleService) {}

    ngOnInit() {
      this.getRoles();
    }
  
  getRoles() {
    this.service.getRoles()
    .subscribe(res=>{
      this.roles = res;
    })
  }
  deleteRole(id) {
    this.service.deleteRole(id).subscribe(res => {
      console.log('Deleted');
    });
  }
}
