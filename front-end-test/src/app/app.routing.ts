import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component'
import { EditRoleComponent } from './roles/components/edit/edit.component';
import {IndexRoleComponent} from './roles/components/index/index.component';
import { CreateRoleComponent } from './roles/components/create/create.component';
export const appRoutes: Routes = [
    { path: 'create', component: CreateRoleComponent },
    {path: 'edit/:id',component: EditRoleComponent},
    {path :'index' , component:IndexRoleComponent},
    // otherwise redirect to home
  //  { path: '**', redirectTo: '' }
];

